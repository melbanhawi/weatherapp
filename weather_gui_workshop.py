#! /usr/bin/python3
from tkinter import *
from PIL import Image, ImageTk

from get_weather import *

class App(Frame):
    def __init__(self, root=None):
        self.root = root
        
        ## configure window 
        self.root.wm_title("Weather")
        self.root.geometry("300x600")

        ## configure labels
        
        # city name
        self.city_label = Label(text="city", fg="Black", font=("Helvetica", 18))
        self.city_label.place(x=100,y=75)
        
        # temp 
        self.temp_label = Label(text=" c", fg="Black", font=("Helvetica", 18))
        self.temp_label.place(x=100,y=225)
        
        # weather
        self.weather_label = Label(text="weather", fg="Black", font=("Helvetica", 18))
        self.weather_label.place(x=100,y=375)
        
        # icon
        image = Image.open('weather.icon')
        imagetk = ImageTk.PhotoImage(image)
        self.weather_icon = Label(image=imagetk)
        self.weather_icon.image = imagetk
        self.weather_icon.place(x=100, y=475)

        self.update_weather()
    
####### MODIFY BELOW ONLY #######

    def update_weather(self):
        
        # get weather data
        weatherJSON = get_weather_data()
        
        # update icon
        get_weather_icon(weatherJSON["weather"][0]["icon"])
        
        # update weather labels 
        # look at how we are accessing the weather icon 
        # this might give you a hint to update city, temp & weather 
        city = "updated_city_name"
        temp_c = "updated temp"
        weather = "updated weather"

        self.city_label.configure(text=city)
        self.temp_label.configure(text=temp_c)
        self.weather_label.configure(text=weather)
        
        # update weather icon
        image = Image.open('weather.icon')
        imagetk = ImageTk.PhotoImage(image)
        self.weather_icon.image = imagetk
        self.weather_icon.configure(image=imagetk)

        self.root.after(1000,self.update_weather) # schedule same method after 1sec

if __name__ == "__main__":
    app=App(Tk())
    app.root.mainloop()
