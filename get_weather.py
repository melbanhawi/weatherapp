#! /usr/bin/python3
import requests
import json

# returns a string with my public ipv4 address
def get_ip():
    ip_response = requests.get("https://checkip.amazonaws.com")
    if ip_response.ok:
        return ip_response.text

# return json string including city, country from ip address
def get_location(ip_address):
    loc_response = requests.get("https://ipinfo.io/"+ip_address+"?token=8e56f0671d72d3")
    if loc_response.ok:
        return loc_response.text

def get_weather(city, region, country):
    weather_response = requests.get("http://api.openweathermap.org/data/2.5/weather?q="+city+","+region+","+country+"&appid=4ee1d0d49a0fe9f98625bc3598f20ca0")
    if weather_response.ok:
        return weather_response.text

def get_weather_icon(code):
    icon_response = requests.get("http://openweathermap.org/img/wn/"+code+"@2x.png")
    if icon_response.ok:
        open("weather.icon", "wb").write(icon_response.content)

def get_weather_data():
    
    ip = get_ip()

    location = get_location(ip)

    locationJSON = json.loads(location)
    
    weather = get_weather(locationJSON["city"], locationJSON["region"], locationJSON["country"])

    weatherJSON = json.loads(weather)

    return weatherJSON

if __name__ == "__main__": 
    
    ip = get_ip()
    print(ip)
    
    location = get_location(ip)
    print (location)
    
    locationJSON = json.loads(location)
    print(locationJSON["city"], locationJSON["country"])
    
    weather = get_weather(locationJSON["city"], locationJSON["region"], locationJSON["country"])
    print(weather)
    weatherJSON = json.loads(weather)
    print(weatherJSON["weather"][0]["description"])

    get_weather_icon(weatherJSON["weather"][0]["icon"])
